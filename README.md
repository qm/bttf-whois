# Back to the Future WHOIS Server

A service to attribute historical BGP announcements. More details and a live version at 
[bttf-whois.measurement.network](https://bttf-whois.measurement.network).

# Compiling

This project is written in Rust and Python. For the Rust parts compile with cargo:

```shell
cargo build --release
```

# Usage

1. This is software, beware and take appropriate safety precautions
2. Install all Python dependencies from `requirements.txt`. A virtual environment is recommended.
3. Create a folder to store raw data downloaded from CAIDA. This will take up approximately 20GiB on disk.
   NVMe storage is recommended for the first import of data, and even then it will still take around 8 hours on a 
   fast machine.
4. Run `python scripts/download-caida.py <your data folder>` to download all source files from CAIDA. 
   This takes a few minutes.
5. Run `./target/release/load_data -d <path to store the SQLite database> -D <your data folder>`.
   This will take a **LONG** time on first import, be patient, leave it running overnight.
6. Run the server: `./target/release/server -d <path to the SQLite database>`. BTTF Whois is now listening on port 43.

## Updating the dataset

To load in new data as CAIDA makes it available perform the following:

1. Run `python scripts/download-caida.py <your data folder>`. This will only download files which are missing from
   the local folder
2. Run `./target/release/load_data -d <path to store the SQLite database> -D <your data folder>`.
   This will only import files which haven't already been loaded into the database. This shouldn't take more than
   a few minutes.

