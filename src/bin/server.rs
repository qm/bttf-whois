#[macro_use]
extern crate log;

use std::str::FromStr;
use async_sqlite::rusqlite::OptionalExtension;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use bttf_whois::ip::IPPrefix;
use clap::Parser;
use futures_util::StreamExt;

const NOTICE: &'static [u8] = include_bytes!("../notice.txt");

#[derive(clap::Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// TCP/IP socket address to listen for connections on
    #[arg(short, long, default_value = "[::]:43")]
    listen_addr: std::net::SocketAddr,

    /// Location of the BTTF WHOIS SQLite database
    #[arg(short, long, default_value = "./db.sqlite")]
    db_path: std::path::PathBuf,

    /// Logging level
    #[arg(short = 'v', long, default_value_t = log::LevelFilter::Info)]
    log_level: log::LevelFilter,

    /// Number of copies of the database to keep in memory at startup
    #[arg(short = 'n', long, default_value_t = 10)]
    db_count: u32,

    /// Maximum number of copies of the database to keep in memory
    #[arg(short = 'm', long, default_value_t = 10)]
    max_db_count: u32,

    /// Log connecting IP addresses
    #[arg(long, default_value_t = false, action)]
    log_ips: bool,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();
    pretty_env_logger::formatted_builder()
        .filter_level(args.log_level)
        .init();

    info!("Starting...");

    let listener = tokio::net::TcpListener::bind(args.listen_addr)
        .await.expect("Unable to bind to socket");
    let db_manager = bttf_whois::in_memory_pool::InMemorySQLitePool::new(args.db_path)
        .await.expect("Unable to open database");
    let db_pool = std::sync::Arc::new(
        bb8::Builder::new()
            .max_size(args.max_db_count)
            .min_idle(args.db_count)
            .max_lifetime(None)
            .idle_timeout(None)
            .test_on_check_out(false)
            .build(db_manager)
            .await.expect("Unable to setup database pool")
    );

    info!("Listening on {}...", args.listen_addr);

    loop {
        let (socket, peer_addr) = match listener.accept().await {
            Ok(s) => s,
            Err(e) => {
                error!("Error accepting connection: {}", e);
                return;
            }
        };
        if args.log_ips {
            info!("New connection from {}", peer_addr);
        }
        let db_pool = db_pool.clone();
        tokio::task::spawn(async move {
            process_socket(socket, db_pool, args.max_db_count as usize).await;
        });
    }
}

async fn read_line<T: tokio::io::AsyncRead + Unpin>(socket: &mut T) -> Option<String> {
    let mut buf: Vec<u8> = vec![];
    loop {
        let byte = match socket.read_u8().await {
            Ok(b) => b,
            Err(_) => {
                return None;
            }
        };
        if byte == 10 { // \n
            break;
        } else if byte == 13 { // \r
            let byte = match socket.read_u8().await {
                Ok(b) => b,
                Err(_) => {
                    return None;
                }
            };
            if byte != 10 { // \n
                return None;
            } else {
                break;
            }
        } else if byte >= 128 {
            return None
        } else {
            buf.push(byte);
        }
    }
    // We checked no byte is over 127, so this is safe
    let line = unsafe { String::from_utf8_unchecked(buf) };
    Some(line)
}

fn read_query_lines<T: tokio::io::AsyncRead + Unpin>(socket: &mut T) -> impl futures_core::stream::Stream<Item = String> + '_ {
    async_stream::stream! {
        let mut socket = tokio::io::BufReader::new(socket);
        let line = match read_line(&mut socket).await {
            Some(l) => l,
            None => return
        };
        if line == "begin" {
            loop {
                let line = match read_line(&mut socket).await {
                    Some(l) => l,
                    None => return
                };
                if line == "end" {
                    return;
                } else {
                    yield line;
                }
            }
        } else {
            yield line;
        }
    }
}

fn read_query<T: tokio::io::AsyncRead + Unpin>(socket: &mut T) -> impl futures_core::stream::Stream<Item = bttf_whois::types::QueryLine> + '_ {
    read_query_lines(socket).map(|l| async move {
        let mut line_parts = l.split(' ');
        let network_str = match line_parts.next() {
            Some(p) => p,
            None => return None
        };
        let network = ipnetwork::IpNetwork::from_str(network_str).ok()
            .or_else(|| {
                Some(match std::net::IpAddr::from_str(network_str).ok()? {
                    std::net::IpAddr::V4(v4) => ipnetwork::Ipv4Network::new(v4, 32).ok()?.into(),
                    std::net::IpAddr::V6(v6) => ipnetwork::Ipv6Network::new(v6, 128).ok()?.into(),
                })
            })?;
        let query_date = line_parts.next().filter(|d| d.len() != 8).and_then(|d| {
            Some(chrono::NaiveDate::from_ymd_opt(
                d[0..4].parse::<i32>().ok()?,
                d[4..6].parse::<u32>().ok()?,
                d[6..8].parse::<u32>().ok()?,
            )?)
        }).unwrap_or_else(|| chrono::Utc::now().date_naive());
        Some(bttf_whois::types::QueryLine {
            network,
            query_date,
        })
    }).buffered(25).filter_map(|o| async { o })
}

async fn process_socket<T: tokio::io::AsyncRead + tokio::io::AsyncWrite + Unpin>(
    mut socket: T, db_pool: std::sync::Arc<bb8::Pool<bttf_whois::in_memory_pool::InMemorySQLitePool>>,
    db_count: usize
) {
    let _ = socket.write(NOTICE).await;
    let conn = match db_pool.get().await {
        Ok(c) => c,
        Err(e) => {
            error!("Unable to get database connection: {}", e);
            let _ = socket.write(b"Internal server error\r\n").await;
            return;
        }
    };

    let most_recent_date_v4: chrono::NaiveDate = match tokio::task::block_in_place(|| {
        conn.query_row("SELECT date FROM processed_files_v4 ORDER BY date DESC LIMIT 1", (), |r| r.get(0))
    }) {
        Ok(r) => r,
        Err(e) => {
            error!("Error querying database: {}", e);
            let _ = socket.write(b"Internal server error\r\n").await;
            return;
        }
    };
    let _ = socket.write(format!("# Most recent IPv4 data: {}\r\n", most_recent_date_v4.format("%Y-%m-%d")).as_bytes()).await;

    let most_recent_date_v6: chrono::NaiveDate = match tokio::task::block_in_place(|| {
        conn.query_row("SELECT date FROM processed_files_v6 ORDER BY date DESC LIMIT 1", (), |r| r.get(0))
    }) {
        Ok(r) => r,
        Err(e) => {
            error!("Error querying database: {}", e);
            let _ = socket.write(b"Internal server error\r\n").await;
            return;
        }
    };
    let _ = socket.write(format!("# Most recent IPv6 data: {}\r\n", most_recent_date_v6.format("%Y-%m-%d")).as_bytes()).await;

    let _ = socket.write(b"# READY\r\n").await;

    let results = match read_query(&mut socket).map(|line| {
        let db_pool = db_pool.clone();
        async move {
            let prefix = line.network.text_prefix();
            let query = if line.network.is_ipv6() {
                "SELECT prefix, from_date, until_date, asn FROM prefix_v6 WHERE prefix = ? AND from_date <= ? AND until_date >= ?"
            } else {
                "SELECT prefix, from_date, until_date, asn FROM prefix_v4 WHERE prefix = ? AND from_date <= ? AND until_date >= ?"
            };

            let conn = db_pool.get().await?;
            let res = tokio::task::block_in_place(|| -> rusqlite::Result<Option<bttf_whois::types::OutputResult>> {
                let mut search_prefix = prefix.as_str();
                if line.network.is_ipv6() {
                    search_prefix = &search_prefix[..std::cmp::min(search_prefix.len(), 48)];
                } else {
                    search_prefix = &search_prefix[..std::cmp::min(search_prefix.len(), 24)];
                }
                let maximum_search = if line.network.is_ipv6() {
                    12
                } else {
                    8
                };

                loop {
                    let res = conn.query_row(query, (search_prefix, line.query_date, line.query_date), |r| {
                        let network = bttf_whois::ip::text_prefix_to_network(&r.get::<_, String>(0).unwrap(), line.network.is_ipv6());
                        let date_last = r.get(2).unwrap();
                        let mut asns: Vec<u32> = r.get::<_, String>(3).unwrap().split(',').into_iter()
                            .map(|a| a.parse::<u32>().unwrap()).collect();
                        asns.sort();

                        let mut as_to_org = vec![];
                        for asn in &asns {
                            if let Some(d) = conn.query_row(
                                "SELECT changed_date, aut_name, org_id, file_date, opaque_id, source \
                            FROM asn WHERE asn = ? AND changed_date <= ? ORDER BY file_date DESC LIMIT 1",
                                (asn, line.query_date), |r| {
                                    let org_id: String = r.get(2)?;

                                    let org = conn.query_row(
                                        "SELECT changed_date, org_name, country, source FROM org \
                                    WHERE org_id = ? AND file_date = ?",
                                        (&org_id, r.get::<_, chrono::NaiveDate>(3)?), |o| {
                                            Ok(bttf_whois::types::Org {
                                                org_id: org_id.clone(),
                                                changed: o.get(0)?,
                                                org_name: o.get(1)?,
                                                country: o.get(2)?,
                                                source: o.get(3)?,
                                            })
                                        },
                                    ).optional()?;

                                    Ok(bttf_whois::types::ASN {
                                        asn: *asn,
                                        name: r.get(1)?,
                                        opaque_id: r.get(4)?,
                                        source: r.get(5)?,
                                        changed: r.get(0)?,
                                        org,
                                    })
                                },
                            ).optional()? {
                                as_to_org.push(d)
                            }
                        }

                        Ok(bttf_whois::types::OutputResult {
                            date_first: r.get(1).unwrap(),
                            date_last: if line.network.is_ipv6() && date_last == most_recent_date_v6 {
                                None
                            } else if line.network.is_ipv4() && date_last == most_recent_date_v4 {
                                None
                            } else {
                                Some(date_last)
                            },
                            prefix: network,
                            asns,
                            as_to_org,
                        })
                    }).optional()?;
                    if let Some(res) = res {
                        return Ok(Some(res));
                    } else {
                        search_prefix = &search_prefix[..search_prefix.len() - 1];
                        if search_prefix.len() < maximum_search {
                            return Ok(None);
                        }
                    }
                }
            }).map_err(|e| bb8::RunError::User(e.into()))?;

            Ok::<_, bb8::RunError<async_sqlite::Error>>(bttf_whois::types::Output {
                ip: line.network,
                query_date: line.query_date,
                results: res,
            })
        }
    }).buffered(db_count).collect::<Vec<Result<bttf_whois::types::Output, _>>>().await
        .into_iter().collect::<Result<Vec<_>, _>>() {
        Ok(r) => r,
        Err(e) => {
            error!("Error querying database: {}", e);
            let _ = socket.write(b"Internal server error\r\n").await;
            return;
        }
    };

    for output in results {
        let output_json = serde_json::to_vec(&output).unwrap();
        let _ = socket.write(&output_json).await;
        let _ = socket.write(b"\r\n").await;
    }
}