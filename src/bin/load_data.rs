use std::fmt::{Display, Formatter, Write};
use std::io::{BufRead};
use chrono::Datelike;
use fallible_iterator::FallibleIterator;
use rusqlite::OptionalExtension;
use clap::Parser;

#[derive(clap::Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Location of the BTTF WHOIS SQLite database
    #[arg(short, long, default_value = "./db.sqlite")]
    db_path: std::path::PathBuf,

    /// Location of raw CAIDA data files.
    /// Must contain the following folders:
    /// `caida_prefix_v4`
    /// `caida_prefix_v6`
    /// `caida_asn`
    /// Files in each folder should be named YYYY-MM-DD.txt.gz
    #[arg(short = 'D', long)]
    data_dir: std::path::PathBuf,
}

trait CAIDAData {
    type File: Send + 'static;

    fn data_dir() -> &'static str;

    fn processed_files_table() -> &'static str;

    fn prefix_table() -> &'static str;

    fn load_file(
        file_index: usize, file_date: chrono::NaiveDate,
        data_root: &std::path::Path,
        multi_progress: std::sync::Arc<indicatif::MultiProgress>,
        tx: std::sync::mpsc::SyncSender<(usize, Self::File)>,
    ) -> Result<(), String>;
}

struct CAIDADataV4 {}

impl CAIDAData for CAIDADataV4 {
    type File = CaidaFile;

    fn data_dir() -> &'static str {
        "caida_prefix_v4"
    }

    fn processed_files_table() -> &'static str {
        "processed_files_v4"
    }

    fn prefix_table() -> &'static str {
        "prefix_v4"
    }

    fn load_file(
        file_index: usize, file_date: chrono::NaiveDate,
        data_root: &std::path::Path,
        multi_progress: std::sync::Arc<indicatif::MultiProgress>,
        tx: std::sync::mpsc::SyncSender<(usize, CaidaFile)>,
    ) -> Result<(), String> {
        load_caida_file::<Self, ipnetwork::Ipv4Network>(file_index, file_date, data_root, multi_progress, tx)
    }
}

struct CAIDADataV6 {}

impl CAIDAData for CAIDADataV6 {
    type File = CaidaFile;

    fn data_dir() -> &'static str {
        "caida_prefix_v6"
    }

    fn processed_files_table() -> &'static str {
        "processed_files_v6"
    }

    fn prefix_table() -> &'static str {
        "prefix_v6"
    }

    fn load_file(
        file_index: usize, file_date: chrono::NaiveDate,
        data_root: &std::path::Path,
        multi_progress: std::sync::Arc<indicatif::MultiProgress>,
        tx: std::sync::mpsc::SyncSender<(usize, CaidaFile)>,
    ) -> Result<(), String> {
        load_caida_file::<Self, ipnetwork::Ipv6Network>(file_index, file_date, data_root, multi_progress, tx)
    }
}

struct CAIDADataASN {}

impl CAIDAData for CAIDADataASN {
    type File = CaidaAsnFile;

    fn data_dir() -> &'static str {
        "caida_asn"
    }

    fn processed_files_table() -> &'static str {
        "processed_files_asn"
    }

    fn prefix_table() -> &'static str {
        unimplemented!();
    }

    fn load_file(
        file_index: usize, file_date: chrono::NaiveDate,
        data_root: &std::path::Path,
        multi_progress: std::sync::Arc<indicatif::MultiProgress>,
        tx: std::sync::mpsc::SyncSender<(usize, CaidaAsnFile)>,
    ) -> Result<(), String> {
        load_caida_asn_file::<Self>(file_index, file_date, data_root, multi_progress, tx)
    }
}

#[derive(Debug)]
struct ASNSet {
    asn: Vec<u32>,
}

impl From<Vec<u32>> for ASNSet {
    fn from(value: Vec<u32>) -> Self {
        Self {
            asn: value
        }
    }
}

impl Display for ASNSet {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for (i, asn) in self.asn.iter().enumerate() {
            if i != 0 {
                f.write_char(',')?;
            }
            f.write_fmt(format_args!("{}", asn))?;
        }
        Ok(())
    }
}

#[derive(Debug)]
struct CaidaLine {
    prefix: String,
    asn: String,
}

#[derive(Debug)]
struct CaidaFile {
    date: chrono::NaiveDate,
    lines: Vec<CaidaLine>,
}

#[derive(Debug)]
struct CaidaOrgLine {
    org_id: Option<String>,
    changed: Option<chrono::NaiveDate>,
    org_name: String,
    country: String,
    source: String,
}

#[derive(Debug)]
struct CaidaAsnLine {
    asn: u32,
    changed: Option<chrono::NaiveDate>,
    aut_name: String,
    org_id: Option<String>,
    opaque_id: String,
    source: String,
}

#[derive(Debug)]
struct CaidaAsnFile {
    date: chrono::NaiveDate,
    org_lines: Vec<CaidaOrgLine>,
    asn_lines: Vec<CaidaAsnLine>,
}

fn main() {
    let args = Args::parse();

    println!("Loading database...");
    let db = rusqlite::Connection::open(args.db_path).unwrap();
    setup_db(&db);

    println!("Finding unprocessed files...");
    let (files_v4, last_date_v4) = caida_files::<CAIDADataV4>(&db, &args.data_dir);
    let (files_v6, last_date_v6) = caida_files::<CAIDADataV6>(&db, &args.data_dir);
    let (files_asn, _) = caida_files::<CAIDADataASN>(&db, &args.data_dir);

    let multi_progress = std::sync::Arc::new(indicatif::MultiProgress::new());

    let progress = multi_progress.add(
        indicatif::ProgressBar::new(files_v4.len() as u64)
            .with_style(indicatif::style::ProgressStyle::with_template(
                "IPv4 Files        {bar} {human_pos}/{human_len} {percent}% done, {elapsed} elapsed of {duration}"
            ).unwrap())
    );
    progress.tick();

    let mut file_recv = load_files::<CAIDADataV4>(files_v4, &args.data_dir, multi_progress.clone());
    write_to_db::<CAIDADataV4>(&db, multi_progress.clone(), &progress, &mut file_recv, last_date_v4);
    progress.finish();

    let progress = multi_progress.add(
        indicatif::ProgressBar::new(files_v6.len() as u64)
            .with_style(indicatif::style::ProgressStyle::with_template(
                "IPv6 Files        {bar} {human_pos}/{human_len} {percent}% done, {elapsed} elapsed of {duration}"
            ).unwrap())
    );
    progress.tick();
    let mut file_recv = load_files::<CAIDADataV6>(files_v6, &args.data_dir, multi_progress.clone());
    write_to_db::<CAIDADataV6>(&db, multi_progress.clone(), &progress, &mut file_recv, last_date_v6);
    progress.finish();

    let progress = multi_progress.add(
        indicatif::ProgressBar::new(files_asn.len() as u64)
            .with_style(indicatif::style::ProgressStyle::with_template(
                "ASN Files         {bar} {human_pos}/{human_len} {percent}% done, {elapsed} elapsed of {duration}"
            ).unwrap())
    );
    progress.tick();
    let mut file_recv = load_files::<CAIDADataASN>(files_asn, &args.data_dir, multi_progress.clone());
    write_to_db_asn(&db, multi_progress.clone(), &progress, &mut file_recv);
    progress.finish();
}

fn setup_db(db: &rusqlite::Connection) {
    db.execute_batch(
        "
        PRAGMA page_size = 65536;
        PRAGMA journal_mode = OFF;
        PRAGMA synchronous = 0;
        PRAGMA cache_size = 1000000;
        PRAGMA locking_mode = EXCLUSIVE;
        PRAGMA temp_store = MEMORY;
        VACUUM;
        BEGIN;
        CREATE TABLE IF NOT EXISTS prefix_v4 (
            id INTEGER PRIMARY KEY,
            prefix VARCHAR(32) NOT NULL,
            from_date DATE NOT NULL,
            until_date DATE NOT NULL,
            asn TEXT
        );
        CREATE INDEX IF NOT EXISTS prefix_v4_prefix ON prefix_v4(prefix COLLATE NOCASE);
        CREATE INDEX IF NOT EXISTS prefix_v4_asn ON prefix_v4(asn);
        CREATE TABLE IF NOT EXISTS processed_files_v4 (
            id INTEGER PRIMARY KEY,
            date DATE NOT NULL
        );
        CREATE TABLE IF NOT EXISTS prefix_v6 (
            id INTEGER PRIMARY KEY,
            prefix VARCHAR(128) NOT NULL,
            from_date DATE NOT NULL,
            until_date DATE NOT NULL,
            asn TEXT
        );
        CREATE INDEX IF NOT EXISTS prefix_v6_prefix ON prefix_v6(prefix COLLATE NOCASE);
        CREATE INDEX IF NOT EXISTS prefix_v6_asn ON prefix_v6(asn);
        CREATE TABLE IF NOT EXISTS processed_files_v6 (
            id INTEGER PRIMARY KEY,
            date DATE NOT NULL
        );
        CREATE TABLE IF NOT EXISTS asn (
            id INTEGER PRIMARY KEY,
            asn INTEGER NOT NULL,
            changed_date DATE NOT NULL,
            aut_name TEXT NOT NULL,
            org_id VARCHAR(255) NULL,
            file_date DATE NOT NULL,
            opaque_id VARCHAR(255) NOT NULL,
            source VARCHAR(32) NOT NULL
        );
        CREATE INDEX IF NOT EXISTS asn_asn ON asn(asn);
        CREATE INDEX IF NOT EXISTS asn_asn_changed ON asn(asn, changed_date);
        CREATE TABLE IF NOT EXISTS org (
            id INTEGER PRIMARY KEY,
            org_id VARCHAR(255) NOT NULL,
            file_date DATE NOT NULL,
            changed_date DATE NOT NULL,
            org_name TEXT NOT NULL,
            country VARCHAR(2) NOT NULL,
            source VARCHAR(32) NOT NULL
        );
        CREATE INDEX IF NOT EXISTS org_org_id_file_date ON org(org_id, file_date);
        CREATE TABLE IF NOT EXISTS processed_files_asn (
            id INTEGER PRIMARY KEY,
            date DATE NOT NULL
        );
        COMMIT;"
    ).expect("Unable to setup database");
}

fn caida_files<D: CAIDAData>(db: &rusqlite::Connection, data_root: &std::path::Path) -> (Vec<chrono::NaiveDate>, Option<chrono::NaiveDate>) {
    let mut existing_files_stmt = db.prepare(&format!("SELECT date FROM {} ORDER BY date", D::processed_files_table()))
        .expect("Unable to setup prepared statement");
    let existing_files_rows = existing_files_stmt.query(()).unwrap();
    let existing_files = existing_files_rows.map(|r| r.get(0)).collect::<Vec<chrono::NaiveDate>>()
        .expect("Unable to get already processed files");

    let last_file = existing_files.last().map(|d| d.to_owned());
    let existing_files = existing_files.into_iter().collect::<std::collections::HashSet<_>>();

    let re = regex::Regex::new(r"^(\d{4})-(\d{2})-(\d{2})\.txt\.gz$").unwrap();

    let mut files = std::fs::read_dir(data_root.join(D::data_dir())).expect("Unable to enumerate CAIDA data directory") // List all files in the data directory
        .map(|e| e.map(|e| e.file_name())) // Get their file name
        .map(|n| n.map(|n| n.to_string_lossy().to_string())) // Convert OsString to a UTF-8 String
        .filter_map(|n| n.map(|n| re.captures(&n).map(|c| { // Run the file name against the regex
            c.iter().filter_map(|m| m.map(|m| m.as_str().to_string())).collect::<Vec<_>>() // Convert regex capture groups to Strings
        })).transpose())
        .map(|c| c.map(|c| (
            c[1].parse::<i32>().unwrap(), c[2].parse::<u32>().unwrap(),
            c[3].parse::<u32>().unwrap()
        ))) // Parse each capture group into integers
        .filter_map(|n| n.map(|(y, m, d)| chrono::NaiveDate::from_ymd_opt(y, m, d)).transpose()) // Integers to a date
        .filter(|d| d.as_ref().map(|d| !existing_files.contains(&d)).unwrap_or(true)) // Filter out files we've already processed
        .collect::<Result<Vec<_>, std::io::Error>>().expect("Unable to get file names");
    files.sort();

    (files, last_file)
}

fn load_files<D: CAIDAData>(files: Vec<chrono::NaiveDate>, data_root: &std::path::Path, multi_progress: std::sync::Arc<indicatif::MultiProgress>) -> bttf_whois::utils::SortingReceiver<D::File> {
    let pool = threadpool::ThreadPool::new(8);

    let (tx, rx) = std::sync::mpsc::sync_channel(0);
    for (i, date) in files.into_iter().enumerate() {
        let p = multi_progress.clone();
        let tx = tx.clone();
        let data_root = data_root.to_path_buf();
        pool.execute(move || {
            if let Err(err) = D::load_file(i, date, &data_root, p, tx) {
                println!("Failed to load CAIDA file for {}: {}", date, err);
                std::process::exit(-1);
            }
        });
    }
    drop(tx);

    let recv = bttf_whois::utils::SortingReceiver::new(rx);

    recv
}

fn load_caida_file<D: CAIDAData, P: bttf_whois::ip::IPPrefix>(
    file_index: usize,
    file_date: chrono::NaiveDate, data_root: &std::path::Path,
    multi_progress: std::sync::Arc<indicatif::MultiProgress>,
    tx: std::sync::mpsc::SyncSender<(usize, CaidaFile)>,
) -> Result<(), String> {
    let progress_style = indicatif::style::ProgressStyle::with_template(
        "Load   {prefix} {bar} {binary_bytes}/{binary_total_bytes}"
    ).unwrap();

    let file_path = data_root.join(D::data_dir()).join(format!(
        "{:04}-{:02}-{:02}.txt.gz",
        file_date.year(), file_date.month(), file_date.day()
    ));
    let fs_meta = std::fs::metadata(&file_path)
        .map_err(|e| format!("Unable to read file metadata - {}", e))?;
    let progress = indicatif::ProgressBar::new(fs_meta.len())
        .with_style(progress_style)
        .with_prefix(file_date.format("%Y-%m-%d").to_string());
    let progress = multi_progress.add(progress);

    let file = std::fs::File::open(&file_path)
        .map_err(|e| format!("Unable to open file - {}", e))?;
    let gzip_reader = flate2::read::GzDecoder::new(file);
    let mut buf_reader = std::io::BufReader::new(gzip_reader);
    let mut buf = String::new();

    let mut lines = vec![];

    loop {
        let bytes_read = buf_reader.read_line(&mut buf)
            .map_err(|e| format!("Unable to read line - {}", e))?;
        if bytes_read == 0 {
            break;
        }
        progress.inc(bytes_read as u64);

        let mut parts = buf.trim().split("\t");
        let prefix = parts.next()
            .ok_or_else(|| "Prefix not found in line".to_string())?;
        let prefix_len = parts.next()
            .ok_or_else(|| "Prefix length not found in line".to_string())?
            .parse::<u8>()
            .map_err(|e| format!("Invalid prefix length - {}", e))?;
        let asn = parts.next()
            .ok_or_else(|| "ASN not found in line".to_string())?
            .split("_")
            .map(|a| -> Result<u32, String> {
                let a = if a.contains(",") {
                    let mut p = a.splitn(2, ",");
                    p.next().unwrap()
                } else {
                    a
                };
                let asn = if a.contains(".") {
                    let mut p = a.splitn(2, ".");
                    let u = p.next()
                        .ok_or_else(|| "16-bit ASN part not found in line".to_string())?
                        .parse::<u16>()
                        .map_err(|e| format!("Invalid ASN - {}", e))?;
                    let l = p.next()
                        .ok_or_else(|| "16-bit ASN part not found in line".to_string())?
                        .parse::<u16>()
                        .map_err(|e| format!("Invalid ASN - {}", e))?;
                    ((u as u32) << 16) + (l as u32)
                } else {
                    a.parse::<u32>()
                        .map_err(|e| format!("Invalid ASN - {}", e))?
                };
                Ok(asn)
            }).collect::<Result<Vec<_>, _>>()?;
        let prefix = P::new(prefix, prefix_len)?;

        lines.push(CaidaLine {
            prefix: prefix.text_prefix(),
            asn: ASNSet::from(asn).to_string(),
        });

        buf.clear()
    }

    tx.send((file_index, CaidaFile {
        date: file_date,
        lines,
    })).unwrap();

    progress.finish();
    multi_progress.remove(&progress);

    Ok(())
}

enum ASNFileState {
    Outside,
    OrgBlock,
    AsnBlock,
    Asn2Block,
}

fn load_caida_asn_file<D: CAIDAData>(
    file_index: usize,
    file_date: chrono::NaiveDate, data_root: &std::path::Path,
    multi_progress: std::sync::Arc<indicatif::MultiProgress>,
    tx: std::sync::mpsc::SyncSender<(usize, CaidaAsnFile)>,
) -> Result<(), String> {
    let progress_style = indicatif::style::ProgressStyle::with_template(
        "Load   {prefix} {bar} {binary_bytes}/{binary_total_bytes}"
    ).unwrap();

    let file_path = data_root.join(D::data_dir()).join(format!(
        "{:04}-{:02}-{:02}.txt.gz",
        file_date.year(), file_date.month(), file_date.day()
    ));
    let fs_meta = std::fs::metadata(&file_path)
        .map_err(|e| format!("Unable to read file metadata - {}", e))?;
    let progress = indicatif::ProgressBar::new(fs_meta.len())
        .with_style(progress_style)
        .with_prefix(file_date.format("%Y-%m-%d").to_string());
    let progress = multi_progress.add(progress);

    let file = std::fs::File::open(&file_path)
        .map_err(|e| format!("Unable to open file - {}", e))?;
    let gzip_reader = flate2::read::GzDecoder::new(file);
    let mut buf_reader = std::io::BufReader::new(gzip_reader);
    let mut buf = String::new();

    let mut org_lines = vec![];
    let mut asn_lines = vec![];

    let mut state = ASNFileState::Outside;

    loop {
        buf.clear();
        let bytes_read = buf_reader.read_line(&mut buf)
            .map_err(|e| format!("Unable to read line - {}", e))?;
        if bytes_read == 0 {
            break;
        }
        progress.inc(bytes_read as u64);

        let l = buf.trim();
        if l.starts_with("# format:") {
            if l == "# format:org_id|changed|org_name|country|source" {
                state = ASNFileState::OrgBlock;
            } else if l == "# format:aut|changed|aut_name|org_id|source" {
                state = ASNFileState::AsnBlock;
            } else if l == "# format:aut|changed|aut_name|org_id|opaque_id|source" {
                state = ASNFileState::Asn2Block;
            } else {
                state = ASNFileState::Outside;
            }
            continue;
        } else if buf.starts_with("#") {
            continue;
        }

        let mut parts = l.split('|');
        match state {
            ASNFileState::Outside => {}
            ASNFileState::OrgBlock => {
                let org_id = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let changed = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let org_name = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let country = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let source = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;

                org_lines.push(CaidaOrgLine {
                    org_id: if org_id.is_empty() { None } else { Some(org_id.to_string()) },
                    changed: if changed.is_empty() { None } else {
                        Some(chrono::NaiveDate::parse_from_str(changed, "%Y%m%d")
                            .map_err(|e| format!("Invalid date - {}", e))?)
                    },
                    org_name: org_name.to_string(),
                    country: country.to_string(),
                    source: source.to_string(),
                });
            }
            ASNFileState::AsnBlock => {
                let asn = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let changed = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let aut_name = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let org_id = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let source = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;

                asn_lines.push(CaidaAsnLine {
                    asn: asn.parse()
                        .map_err(|e| format!("Invalid ASN - {}", e))?,
                    changed: if changed.is_empty() { None } else {
                        Some(chrono::NaiveDate::parse_from_str(changed, "%Y%m%d")
                            .map_err(|e| format!("Invalid date - {}", e))?)
                    },
                    aut_name: aut_name.to_string(),
                    org_id: if org_id.is_empty() { None } else { Some(org_id.to_string()) },
                    opaque_id: String::new(),
                    source: source.to_string(),
                });
            }
            ASNFileState::Asn2Block => {
                let asn = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let changed = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let aut_name = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let org_id = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let opaque_id = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;
                let source = parts.next()
                    .ok_or_else(|| "Missing part".to_string())?;

                asn_lines.push(CaidaAsnLine {
                    asn: asn.parse()
                        .map_err(|e| format!("Invalid ASN - {}", e))?,
                    changed: if changed.is_empty() { None } else {
                        Some(chrono::NaiveDate::parse_from_str(changed, "%Y%m%d")
                            .map_err(|e| format!("Invalid date - {}", e))?)
                    },
                    aut_name: aut_name.to_string(),
                    org_id: if org_id.is_empty() { None } else { Some(org_id.to_string()) },
                    opaque_id: opaque_id.to_string(),
                    source: source.to_string(),
                });
            }
        }
    }

    tx.send((file_index, CaidaAsnFile {
        date: file_date,
        org_lines,
        asn_lines,
    })).unwrap();

    progress.finish();
    multi_progress.remove(&progress);

    Ok(())
}

fn write_to_db<D: CAIDAData>(
    db: &rusqlite::Connection, multi_progress: std::sync::Arc<indicatif::MultiProgress>,
    progress: &indicatif::ProgressBar, file_recv: &mut bttf_whois::utils::SortingReceiver<CaidaFile>,
    mut last_date: Option<chrono::NaiveDate>,
) {
    let mut query_stmt = db.prepare(&format!("SELECT id FROM {} WHERE prefix = ? AND until_date = ? AND asn = ?", D::prefix_table()))
        .expect("Unable to setup prepared statement");
    let mut update_stmt = db.prepare(&format!("UPDATE {} SET until_date = ? WHERE id = ?", D::prefix_table()))
        .expect("Unable to setup prepared statement");
    let mut insert_stmt = db.prepare(&format!("INSERT INTO {} (prefix, from_date, until_date, asn) VALUES (?, ?, ?, ?)", D::prefix_table()))
        .expect("Unable to setup prepared statement");
    let mut insert_file_stmt = db.prepare(&format!("INSERT INTO {} (date) VALUES (?)", D::processed_files_table()))
        .expect("Unable to setup prepared statement");

    while let Some(file) = file_recv.recv() {
        let file_progress = multi_progress.add(indicatif::ProgressBar::new(file.lines.len() as u64)
            .with_style(indicatif::style::ProgressStyle::with_template(
                "Ingest {prefix} {bar} {human_pos}/{human_len} {percent}% done, {elapsed} elapsed of {duration}"
            ).unwrap())
            .with_prefix(file.date.format("%Y-%m-%d").to_string()));
        file_progress.tick();

        let (update_tx, update_rx) = std::sync::mpsc::channel();
        let (insert_tx, insert_rx) = std::sync::mpsc::channel();

        for line in file.lines {
            let existing_row_id: Option<u64> = if let Some(ref last_date) = last_date {
                query_stmt.query_row((&line.prefix, last_date, &line.asn), |r| r.get(0))
                    .optional().expect("Unable to lookup existing row")
            } else {
                None
            };

            if let Some(id) = existing_row_id {
                update_tx.send((file.date, id)).unwrap();
            } else {
                insert_tx.send((line.prefix, file.date, file.date, line.asn)).unwrap();
            }
            file_progress.inc(1);
        }
        drop(update_tx);
        drop(insert_tx);
        file_progress.finish();

        db.execute("BEGIN TRANSACTION", ()).unwrap();

        for update in update_rx {
            update_stmt.execute(update).unwrap();
        }
        for insert in insert_rx {
            insert_stmt.execute(insert).unwrap();
        }

        insert_file_stmt.execute((file.date,)).unwrap();

        db.execute("END TRANSACTION", ()).unwrap();

        multi_progress.remove(&file_progress);

        last_date = Some(file.date);
        progress.inc(1);
    }
}

fn write_to_db_asn(
    db: &rusqlite::Connection, multi_progress: std::sync::Arc<indicatif::MultiProgress>,
    progress: &indicatif::ProgressBar, file_recv: &mut bttf_whois::utils::SortingReceiver<CaidaAsnFile>,
) {
    let mut query_asn_stmt = db.prepare("SELECT id FROM asn WHERE asn = ? AND changed_date = ?")
        .expect("Unable to setup prepared statement");
    let mut changed_date_asn_stmt = db.prepare("SELECT changed_date FROM asn WHERE asn = ? AND changed_date <= ? AND aut_name = ? AND org_id = ? AND source = ? ORDER BY changed_date DESC LIMIT 1")
        .expect("Unable to setup prepared statement");
    let mut insert_asn_stmt = db.prepare("INSERT INTO asn (asn, changed_date, aut_name, org_id, file_date, opaque_id, source) VALUES (?, ?, ?, ?, ?, ?, ?)")
        .expect("Unable to setup prepared statement");
    let mut insert_org_stmt = db.prepare("INSERT INTO org (org_id, file_date, changed_date, org_name, country, source) VALUES (?, ?, ?, ?, ?, ?)")
        .expect("Unable to setup prepared statement");
    let mut insert_file_stmt = db.prepare("INSERT INTO processed_files_asn (date) VALUES (?)")
        .expect("Unable to setup prepared statement");

    while let Some(file) = file_recv.recv() {
        let file_progress = multi_progress.add(indicatif::ProgressBar::new(
            file.asn_lines.len() as u64 + file.org_lines.len() as u64
        ).with_style(indicatif::style::ProgressStyle::with_template(
            "Ingest {prefix} {bar} {human_pos}/{human_len} {percent}% done, {elapsed} elapsed of {duration}"
        ).unwrap()).with_prefix(file.date.format("%Y-%m-%d").to_string()));
        file_progress.tick();

        let (insert_asn_tx, insert_asn_rx) = std::sync::mpsc::channel();
        let (insert_org_tx, insert_org_rx) = std::sync::mpsc::channel();

        for asn_line in file.asn_lines {
            let changed_date = match asn_line.changed {
                Some(d) => d,
                None => {
                    changed_date_asn_stmt.query_row((asn_line.asn, file.date, &asn_line.aut_name, &asn_line.org_id, &asn_line.source), |r| r.get(0))
                        .optional().expect("Unable to query changed date")
                        .unwrap_or(file.date)
                }
            };

            let existing_id: Option<u64> = query_asn_stmt.query_row((asn_line.asn, changed_date), |r| r.get(0))
                .optional().expect("Unable to lookup existing row");

            file_progress.inc(1);

            if existing_id.is_some() {
                continue;
            }

            insert_asn_tx.send((asn_line.asn, changed_date, asn_line.aut_name, asn_line.org_id, file.date, asn_line.opaque_id, asn_line.source)).unwrap();
        }

        for org_line in file.org_lines {
            let org_id = match org_line.org_id {
                Some(i) => i,
                None => continue
            };

            let changed_date = match org_line.changed {
                Some(d) => d,
                None => file.date
            };

            file_progress.inc(1);
            insert_org_tx.send((org_id, file.date, changed_date, org_line.org_name, org_line.country, org_line.source)).unwrap();
        }

        drop(insert_asn_tx);
        drop(insert_org_tx);
        file_progress.finish();

        db.execute("BEGIN TRANSACTION", ()).unwrap();

        for insert in insert_asn_rx {
            insert_asn_stmt.execute(insert).unwrap();
        }
        for insert in insert_org_rx {
            insert_org_stmt.execute(insert).unwrap();
        }

        insert_file_stmt.execute((file.date,)).unwrap();

        db.execute("END TRANSACTION", ()).unwrap();

        multi_progress.remove(&file_progress);
        progress.inc(1);
    }
}
