pub struct QueryLine {
    pub network: ipnetwork::IpNetwork,
    pub query_date: chrono::NaiveDate
}

#[derive(serde::Serialize)]
pub struct Output {
    #[serde(rename = "IP")]
    pub ip: ipnetwork::IpNetwork,
    #[serde(rename = "QDATE", with = "date_format")]
    pub query_date: chrono::NaiveDate,
    pub results: Option<OutputResult>
}

#[derive(serde::Serialize)]
pub struct OutputResult {
    #[serde(rename = "DATA_FIRST", with = "date_format")]
    pub date_first: chrono::NaiveDate,
    #[serde(default, rename = "DATA_LAST", with = "date_format_opt")]
    pub date_last: Option<chrono::NaiveDate>,
    pub asns: Vec<u32>,
    pub prefix: ipnetwork::IpNetwork,
    #[serde(rename = "as2org")]
    pub as_to_org: Vec<ASN>
}

#[derive(serde::Serialize)]
pub struct ASN {
    #[serde(rename = "ASN")]
    pub asn: u32,
    #[serde(rename = "ASNAME")]
    pub name: String,
    #[serde(rename = "RIR")]
    pub source: String,
    #[serde(with = "date_format")]
    pub changed: chrono::NaiveDate,
    pub opaque_id: String,
    pub org: Option<Org>
}

#[derive(serde::Serialize)]
pub struct Org {
    #[serde(rename = "CC")]
    pub country: String,
    #[serde(rename = "RIR")]
    pub source: String,
    #[serde(rename = "ASORG")]
    pub org_name: String,
    pub org_id: String,
    #[serde(with = "date_format")]
    pub changed: chrono::NaiveDate,
}

mod date_format {
    const FORMAT: &'static str = "%Y%m%d";

    pub fn serialize<S: serde::Serializer>(
        date: &chrono::NaiveDate,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        serializer.serialize_str(&format!("{}", date.format(FORMAT)))
    }
}

mod date_format_opt {
    const FORMAT: &'static str = "%Y%m%d";

    pub fn serialize<S: serde::Serializer>(
        date: &Option<chrono::NaiveDate>,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        match date {
            Some(date) => serializer.serialize_str(&format!("{}", date.format(FORMAT))),
            None => serializer.serialize_none()
        }
    }
}