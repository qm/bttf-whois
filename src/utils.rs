pub struct SortingReceiver<X> {
    index: usize,
    rx: std::sync::mpsc::Receiver<(usize, X)>,
    store: std::collections::BTreeMap<usize, X>,
}

impl<X> SortingReceiver<X> {
    pub fn new(rx: std::sync::mpsc::Receiver<(usize, X)>) -> Self {
        Self {
            index: 0,
            rx,
            store: std::collections::BTreeMap::new(),
        }
    }

    pub fn recv(&mut self) -> Option<X> {
        loop {
            if let Some(v) = self.store.remove(&self.index) {
                self.index += 1;
                return Some(v);
            } else {
                match self.rx.recv() {
                    Ok(v) => {
                        if v.0 == self.index {
                            self.index += 1;
                            return Some(v.1);
                        } else {
                            self.store.insert(v.0, v.1);
                        }
                    }
                    Err(_) => {
                        return None;
                    }
                }
            }
        }
    }
}