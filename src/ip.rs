use std::convert::TryInto;
use std::str::FromStr;

pub trait IPPrefix {
    fn new(prefix: &str, prefix_len: u8) -> Result<Self, String> where Self: Sized;

    fn text_prefix(&self) -> String;
}

macro_rules! text_prefix {
    () => {
        #[inline(always)]
        fn text_prefix(&self) -> String {
            let prefix = self.prefix() as usize;
            let net = self.network().octets();
            let mut out = String::with_capacity(prefix);
            let net_bit: &bitvec::slice::BitSlice<u8, bitvec::order::Msb0> = bitvec::slice::BitSlice::from_slice(&net);
            for bit in &net_bit[..prefix] {
                if bit == true {
                    out.push('1');
                } else {
                    out.push('0');
                }
            }
            out
        }
    };
}

impl IPPrefix for ipnetwork::Ipv4Network {
    #[inline(always)]
    fn new(prefix: &str, prefix_len: u8) -> Result<Self, String> {
        let prefix = std::net::Ipv4Addr::from_str(prefix)
            .map_err(|e| format!("Invalid IPv4 address - {}", e))?;
        ipnetwork::Ipv4Network::new(prefix, prefix_len)
            .map_err(|e| format!("Invalid IPv4 network - {}", e))
    }

    text_prefix!();
}

impl IPPrefix for ipnetwork::Ipv6Network {
    #[inline(always)]
    fn new(prefix: &str, prefix_len: u8) -> Result<Self, String> {
        let prefix = std::net::Ipv6Addr::from_str(prefix)
            .map_err(|e| format!("Invalid IPv6 address - {}", e))?;
        ipnetwork::Ipv6Network::new(prefix, prefix_len)
            .map_err(|e| format!("Invalid IPv6 network - {}", e))
    }

    text_prefix!();
}

impl IPPrefix for ipnetwork::IpNetwork {
    #[inline(always)]
    fn new(prefix: &str, prefix_len: u8) -> Result<Self, String> {
        let prefix = std::net::IpAddr::from_str(prefix)
            .map_err(|e| format!("Invalid IP address - {}", e))?;
        ipnetwork::IpNetwork::new(prefix, prefix_len)
            .map_err(|e| format!("Invalid IP network - {}", e))
    }

    #[inline(always)]
    fn text_prefix(&self) -> String {
        match self {
            ipnetwork::IpNetwork::V4(n) => n.text_prefix(),
            ipnetwork::IpNetwork::V6(n) => n.text_prefix(),
        }
    }
}

pub fn text_prefix_to_network(text_prefix: &str, is_ipv6: bool) -> ipnetwork::IpNetwork {
    if is_ipv6 && text_prefix.len() > 128 {
        panic!("Invalid prefix");
    } else if !is_ipv6 && text_prefix.len() > 32 {
        panic!("Invalid prefix");
    }
    let mut net_vec = if is_ipv6 {
        vec![0u8; 16]
    } else {
        vec![0u8; 4]
    };
    let net_bit: &mut bitvec::slice::BitSlice<u8, bitvec::order::Msb0> = bitvec::slice::BitSlice::from_slice_mut(&mut net_vec);
    for (i, bit) in text_prefix.chars().into_iter().enumerate() {
        if bit == '0' {
            net_bit.set(i, false);
        } else if  bit == '1' {
            net_bit.set(i, true);
        }
    }
    if is_ipv6 {
        let net: [u8; 16] = net_vec.try_into().unwrap();
        let addr = std::net::Ipv6Addr::from(net);
        ipnetwork::IpNetwork::V6(ipnetwork::Ipv6Network::new(addr, text_prefix.len() as u8).unwrap())
    } else {
        let net: [u8; 4] = net_vec.try_into().unwrap();
        let addr = std::net::Ipv4Addr::from(net);
        ipnetwork::IpNetwork::V4(ipnetwork::Ipv4Network::new(addr, text_prefix.len() as u8).unwrap())
    }
}