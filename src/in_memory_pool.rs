use log::info;

pub struct InMemorySQLitePool {
    source_db: async_sqlite::Client,
}

impl InMemorySQLitePool {
    pub async fn new<P: AsRef<std::path::Path>>(path: P) -> Result<Self, async_sqlite::Error> {
        let conn = async_sqlite::ClientBuilder::new()
            .path(path)
            .open().await?;
        Ok(Self {
            source_db: conn
        })
    }
}

#[async_trait::async_trait]
impl bb8::ManageConnection for InMemorySQLitePool {
    type Connection = rusqlite::Connection;
    type Error = async_sqlite::Error;

    async fn connect(&self) -> Result<Self::Connection, Self::Error> {
        let mut new_db = rusqlite::Connection::open_in_memory()?;
        let new_db = self.source_db.conn(move |conn| {
            info!("Copying SQLite database into in-memory database");
            let now = std::time::SystemTime::now();
            let backup = rusqlite::backup::Backup::new(conn, &mut new_db)?;
            backup.step(-1)?;
            drop(backup);
            info!("Created in memory database, took {}s", now.elapsed().unwrap().as_secs());
            Ok(new_db)
        }).await?;
        Ok(new_db)
    }

    async fn is_valid(&self, conn: &mut Self::Connection) -> Result<(), Self::Error> {
        tokio::task::block_in_place(|| {
            conn.query_row("SELECT 1", (), |_| { Ok(()) })?;
            Ok(())
        })
    }

    fn has_broken(&self, _conn: &mut Self::Connection) -> bool {
        false
    }
}