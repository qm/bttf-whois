import re
import tqdm
import requests
import pathlib
import os.path
import bs4
import concurrent.futures
import argparse

CAIDA_V4_ROOT = "https://publicdata.caida.org/datasets/routing/routeviews-prefix2as/"
CAIDA_V6_ROOT = "https://publicdata.caida.org/datasets/routing/routeviews6-prefix2as/"
CAIDA_ASN_ROOT = "https://publicdata.caida.org/datasets/as-organizations/"

YEAR_RE = re.compile(r"^(\d{4})/$")
MONTH_RE = re.compile(r"^(\d{2})/$")
FILE_RE = re.compile(r"^routeviews-rv\d-(\d{4})(\d{2})(\d{2})-\d{4}\.pfx2as\.gz$")
ASN_FILE_RE = re.compile(r"^(\d{4})(\d{2})(\d{2})\.as-org2info\.txt\.gz$")
CAIDA_DISK_RE = re.compile(r"^(\d{4})-(\d{2})-(\d{2})\.txt\.gz$")

def get_present_caida_versions(folder: pathlib.Path) -> set[tuple[int, int, int]]:
    versions = set()
    for path in folder.glob("*"):
        if m := CAIDA_DISK_RE.match(path.name):
            year, month, day = m.groups()
            key = (int(year), int(month), int(day))
            versions.add(key)

    return versions

def get_caida_versions_asn() -> tuple[set[tuple[int, int, int]], dict[tuple[int, int, int], str]]:
    versions = set()
    file_names = dict()

    r = requests.get(CAIDA_ASN_ROOT)
    r.raise_for_status()
    soup = bs4.BeautifulSoup(r.text, "html.parser")
    for link in soup.find_all('a'):
        href = link.get('href')
        if m := ASN_FILE_RE.match(href):
            year, month, day = m.groups()
            date = (int(year), int(month), int(day))
            versions.add(date)
            file_names[date] = href

    return versions, file_names

def get_caida_versions(root: str) -> tuple[set[tuple[int, int, int]], dict[tuple[int, int, int], str]]:
    years = set()

    r = requests.get(root)
    r.raise_for_status()
    soup = bs4.BeautifulSoup(r.text, "html.parser")
    for link in soup.find_all('a'):
        if m := YEAR_RE.match(link.text):
            year = m.groups()[0]
            years.add(int(year))

    months = set()

    for year in tqdm.tqdm(list(years), desc="Years"):
        r = requests.get(root + f"{year:04d}/")
        r.raise_for_status()
        soup = bs4.BeautifulSoup(r.text, "html.parser")
        for link in soup.find_all('a'):
            if m := MONTH_RE.match(link.text):
                month = m.groups()[0]
                months.add((year, int(month)))

    files = set()
    file_names = dict()

    with concurrent.futures.ThreadPoolExecutor(max_workers=32) as pool:
        for r in tqdm.tqdm(pool.map(lambda m: requests.get(root + f"{m[0]:04d}/{m[1]:02d}/"), list(months)), total=len(months), desc="Months"):
            r.raise_for_status()
            soup = bs4.BeautifulSoup(r.text, "html.parser")
            for link in soup.find_all('a'):
                if m := FILE_RE.match(link.text):
                    href = link['href']
                    year, month, day = m.groups()
                    key = (int(year), int(month), int(day))
                    files.add(key)
                    file_names[key] = f"/{year}/{month}/{href}"

    return files, file_names

def download_file(root: str, data_dir: pathlib.Path, version: tuple[int, int, int], file_names: dict[tuple[int, int, int], str]):
    source_url = root + file_names[version]
    target_file = f"{version[0]:04d}-{version[1]:02d}-{version[2]:02d}.txt.gz"
    r = requests.get(source_url, stream=True)
    r.raise_for_status()
    with open(data_dir / target_file, "wb") as f:
        for c in r.iter_content(1024):
            f.write(c)
def main():
    parser = argparse.ArgumentParser(description="Downloads new data files from the CAIDA repository")
    parser.add_argument('data_dir', type=pathlib.Path, help="The root directory to store downloaded CAIDA files in")
    args = parser.parse_args()

    caida_data_dir_v4 = args.data_dir / "caida_prefix_v4"
    caida_data_dir_v6 = args.data_dir / "caida_prefix_v6"
    caida_data_dir_asn = args.data_dir / "caida_asn"

    os.makedirs(caida_data_dir_v4, exist_ok=True)
    os.makedirs(caida_data_dir_v6, exist_ok=True)
    os.makedirs(caida_data_dir_asn, exist_ok=True)

    current_versions_v4, file_names_v4 = get_caida_versions(CAIDA_V4_ROOT)
    current_versions_v6, file_names_v6 = get_caida_versions(CAIDA_V6_ROOT)
    current_versions_asn, file_names_asn = get_caida_versions_asn()
    present_versions_v4 = get_present_caida_versions(caida_data_dir_v4)
    present_versions_v6 = get_present_caida_versions(caida_data_dir_v6)
    present_versions_asn = get_present_caida_versions(caida_data_dir_asn)
    to_download_v4 = list(current_versions_v4 - present_versions_v4)
    to_download_v6 = list(current_versions_v6 - present_versions_v6)
    to_download_asn = list(current_versions_asn - present_versions_asn)

    with concurrent.futures.ThreadPoolExecutor(max_workers=32) as pool:
        p = tqdm.tqdm(total=len(to_download_v4), desc="CAIDA IPv4 Files", unit="file", leave=True, position=1)
        for _ in pool.map(lambda f: download_file(CAIDA_V4_ROOT, caida_data_dir_v4, f, file_names_v4), to_download_v4):
            p.update(1)

        p = tqdm.tqdm(total=len(to_download_v6), desc="CAIDA IPv6 Files", unit="file", leave=True, position=1)
        for _ in pool.map(lambda f: download_file(CAIDA_V6_ROOT, caida_data_dir_v6, f, file_names_v6), to_download_v6):
            p.update(1)

        p = tqdm.tqdm(total=len(to_download_asn), desc="CAIDA ASN Files", unit="file", leave=True, position=1)
        for _ in pool.map(lambda f: download_file(CAIDA_ASN_ROOT, caida_data_dir_asn, f, file_names_asn), to_download_asn):
            p.update(1)

if __name__ == '__main__':
    main()